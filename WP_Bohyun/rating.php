<?php
require_once('./model/Rate.php');
$objRate = new Rate();
$posts = $objRate->getPosts();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="js/menu.js" type="text/javascript"></script>
        <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
        <script src="js/core.js" type="text/javascript"></script>
        <link href="css/core.css" rel="stylesheet" type="text/css"/>
        <title>Rating</title>
    </head>
    <body>
         <div class="navigation">
            <ul class="nav">
                <li>
                    <a href="Home.html">Join Us</a>
                    <ul>
                        <li><a href="login.php">Log In</a></li>
                        <li><a href="logout.php">Log Out</a></li>
                    </ul>
                </li>
                <li>
                    <a href="explored.php">Explored</a>
                    <ul>
                        <li><a href="map.html">Map</a></li>
                        <li><a href="rating.php">Rating</a></li>
                        <li><a href="view_image.php">Image</a></li>
                        <li><a href="user.php">User</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                    <ul>
                        <li><a href="comment.php">Leave Us a Comment</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        
        
        
        
        
        <div id="wrapper">

            <p><a href="#" class="reset">Reset</a></p>

            <div id="comments">

                <?php if (!empty($posts)) { ?>

                    <?php foreach ($posts as $row) { ?>

                        <div class="comment">
                            <span class="name">
                                Posted by <?php echo htmlentities(stripslashes($row['full_name'])); ?> on <time datetime="<?php echo date('Y-m-d', strtotime($row['date'])); ?>"><?php echo $row['date_formatted']; ?></time>
                            </span>
                            <p><?php echo htmlentities(stripslashes($row['comment'])); ?></p>
                            <?php echo $objRate->buttonSet($row['id']); ?>
                        </div>

                    <?php } ?>

                <?php } else { ?>
                    <p>There are currently no comments.</p>
                <?php } ?>

            </div>

        </div>

    </body>
</html>
